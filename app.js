//----------CREATING AND APPENDING TO A TEXT FILE----------
// const fs = require('fs')
// fs.writeFileSync('notes.txt', 'My name is Ashritha.')
// fs.appendFileSync('notes.txt', '\nIam learning Node.js!.')

//----------VALIDATOR USAGE----------
// const validator = require('validator')
// console.log(validator.isEmail('ashritha.shetty@maiora.co'));
// console.log(validator.isURL('thefitsocials.com'));

//----------CHALK USAGE TO PRINT TEXTS IN DIFFERENT COLOR----------
const chalk = require('chalk');
// console.log(chalk.blue('SUCCESS!'));
// console.log(chalk.green.inverse.italic('SUCCESS!'));

const notes = require('./notes');
// const msg = getNotes();
// console.log(msg);

const yargs = require('yargs');
const { argv } = require('yargs');
//-------- For Runing "node app.js Ashritha" OR "node app.js add --title="things to buy""---------
// console.log(process.argv)
// console.log(yargs.argv)
// const command = process.argv[2]
// if(command === 'add') {
//     console.log("Adding Note!")
// } else if(command === 'remove') {
//     console.log("Removing Note!")
// }


//------------------------------------------------NOTES-APP-------------------------------------------------
//Adding, Removing, Listing, Reading

//Creating -----"node app.js add"-----
yargs.command({
    'command': 'add',
    'describe': 'Add a new note',
    'builder': {
        'title': {
            'describe': 'Note Title',
            // ----"node app.js add --title="Shopping List""----
            'demandOption': true,
            'type': 'string'
        },
        'body': {
            'describe': 'Note Body',
            // ----"node app.js add --title="Shopping List" --body="Body""----
            'demandOption': true,
            'type': 'string'
        }
    },
    handler: function (argv) {
        // console.log("Title: " + argv.title + "\nBody: " + argv.body)
        notes.addNote(argv.title, argv.body);
    }
})

//Removing -----"node app.js remove"-----
yargs.command({
    'command': 'remove',
    'describe': 'Removing a note',
    'builder': {
        'title': {
            'description': 'Note Title',
            'demandOption': true,
            'type': 'string'
        }
    },
    handler: function (argv) {
        // console.log("Removing a Note!")
        notes.removeNote(argv.title);
    }
})

//Listing -----"node app.js list"-----
yargs.command({
    'command': 'list',
    'describe': 'Listing the notes',
    handler: function () {
        // console.log("Listing the Notes!")
        notes.listNote()
    }
})

//Reading -----"node app.js read"-----
yargs.command({
    'command': 'read',
    'describe': 'Reading the notes',
    'builder': {
        'title': {
            'description': 'Reading',
            'demandOption': true,
            'type': 'string'
        }
    },
    handler: function (argv) {
        // console.log("Reading the Notes!")
        notes.readNote(argv.title);
    }
})

console.log(yargs.argv)