const fs = require('fs');
const chalk = require('chalk');
const getNotes = function() {
    return 'Your Notes ....'
}

const addNote = function(title, body) {
    const notes = loadNotes()
    // const duplicatNotes = notes.filter(function(note) {
    //     return note.title === title
    // })
    const duplicatNotes = notes.find((note) => note.title === title)
    if(!duplicatNotes) {
        notes.push({
            'title': title,
            'body': body
        })
        // console.log(notes);
        saveNotes(notes);
        console.log(chalk.green.inverse("Note Added"))
    } else {
        console.log(chalk.red.inverse("Note Title already Taken"))
    }
}

const removeNote = function(title) {
    // console.log(title);
    const notes = loadNotes()
    const notesToKeep = notes.filter(function(note) {
        return note.title !== title
    })
    if(notes.length > notesToKeep.length) {
        console.log(chalk.green.inverse("NOTE REMOVED!"))
        saveNotes(notesToKeep)
    }else{
        console.log(chalk.red.inverse("No NOTE Found!"))
    }
}

const listNote = () => {
    const notes = loadNotes()
    console.log(chalk.inverse('Your Notes'))
    notes.forEach((note) => {
        console.log(note.title)
    })
}

const readNote = (title) => {
    const notes = loadNotes()
    const note = notes.find((note) => note.title === title)
    if(note){
        console.log(chalk.inverse(note.title));
        console.log(note.body);
    } else {
        console.log(chalk.red.inverse("NOTE NOT FOUND"));
    }
}

const saveNotes = function(notes) {
    const dataJson = JSON.stringify(notes);
    fs.writeFileSync('notes.json', dataJson);
}

const loadNotes = function() {
    try{
        const dataBuffer = fs.readFileSync('notes.json');
        const dataJson = dataBuffer.toString()
        return JSON.parse(dataJson);
    } catch(e){
        return []
    }
}

module.exports = {
    'getNotes': getNotes,
    'addNote': addNote,
    'removeNote': removeNote,
    'listNote': listNote,
    'readNote': readNote
}