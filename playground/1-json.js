//-----------------node playground/1-json.js----------------------
const fs = require('fs');
// const book = {
//     'title': 'How to Win Friends and Influence People',
//     'author': 'Dale Carnigie'
// }

// const bookJson = JSON.stringify(book)
// console.log(bookJson);
// const bookParsed = JSON.parse(bookJson)
// console.log(bookParsed.author);

// fs.writeFileSync('1-json.json', bookJson);
const dataBuffer = fs.readFileSync('1-json.json')
const dataJSON = dataBuffer.toString();
const data = JSON.parse(dataJSON);
data.name = 'Shetty',
data.age = 21
const dataJSON2 = JSON.stringify(data)
fs.writeFileSync('1-json.json', dataJSON2);

